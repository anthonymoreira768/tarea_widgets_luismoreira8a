import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  final TextStyle estiloTexto =  const TextStyle( fontSize: 25);
  final conteo=10; 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Titulo'),
        centerTitle: true,
        elevation: 1.2,
      ),
      body: Center(
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Text('Numero de clicks',style: TextStyle(fontSize: 25),),
            //Text('0',style: TextStyle(fontSize: 25),)
            Text('Numero de clicks',style: estiloTexto),
            Text('$conteo',style:estiloTexto)

          ],
        )
      ),
      
      floatingActionButton: FloatingActionButton(
        
        child: const Icon(Icons.add),
        
        onPressed: (){
          print('Hola mundo');
          //conteo = conteo+1;
        }),
        
    );
  }
}