import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  const ContadorPage({Key? key}) : super(key: key);

  @override
  State<ContadorPage> createState() => _ContadorPageState();
}

class _ContadorPageState extends State<ContadorPage> {
  final TextStyle _estiloTexto =  const TextStyle( fontSize: 25);
  int _conteo=0; 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('TituloFull'),
        centerTitle: true,
        elevation: 1.2,
      ),
      body: Center(
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Text('Numero de clicks',style: TextStyle(fontSize: 25),),
            //Text('0',style: TextStyle(fontSize: 25),)
            Text('Numero de taps',style: _estiloTexto),
            Text('$_conteo',style:_estiloTexto)

          ],
        )
      ),
      

      /*floatingActionButton: FloatingActionButton(
        
        child: const Icon(Icons.add),
        
        onPressed: (){
          //print('Hola mundo');
          _conteo++;
          setState(() {
            
          });
        }),
      */  
      floatingActionButton: _crearBotones() ,
    );
  }

Widget _crearBotones() {

  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children:<Widget> [
    const SizedBox(width: 30,),
    FloatingActionButton(onPressed:_reset,child: Icon (Icons.exposure_zero)),
    const Expanded(child: SizedBox()),
    FloatingActionButton(child: Icon (Icons.remove), onPressed: _sustraer),
    const SizedBox(width: 5.0,),
    FloatingActionButton(child: Icon (Icons.add), onPressed: _agregar),
    
    ],
  );
  }

  void _agregar() {
    setState(() => _conteo++ );
  }

  void _sustraer () {
    setState(()=>_conteo--);
  }

  void _reset () {
    setState(()=> _conteo=0);
  }
}
  
  
  
